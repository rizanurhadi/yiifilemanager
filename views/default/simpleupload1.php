<h1>Simple Upload Form 1</h1>

<p>max file size : <?php echo $max_filesize ?></p>

<?php $form=$this->beginWidget('CActiveForm',
    array(
        'id' => 'upload-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
	
<div class="row">
	<?php echo CHtml::fileField('myfile') ?>	
</div>
<div>
	<?php echo CHtml::submitButton('Uploads',array("class"=>"btn btn-primary")); ?>
</div>
<?php $this->endWidget(); ?>

<div id="message">
	
</div>

<?php Yii::app()->clientScript->registerScript('search', "
	$('#myfile').bind('change', function() {
		//this.files[0].size gets the size of your file.
		var myfilesize = (this.files[0].size / 1024).toFixed(2);
		var fileExtension = this.files[0].name.split('.')[this.files[0].name.split('.').length - 1].toLowerCase();
		var txt = 'File type : ' +fileExtension + ' <br />';
		if(this.files[0].size > (1024 * 1024)){
            txt += 'Size: ' + (this.files[0].size / (1024*1024)).toFixed(2) + ' MB ';
        } else {
			txt += 'Size: ' + (this.files[0].size / 1024).toFixed(2) + ' KB ';
        };
		$('#message').html(txt);
	});
"); ?>