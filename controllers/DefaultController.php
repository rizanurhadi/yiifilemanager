<?php

class DefaultController extends Controller
{
	public $layout = '//layouts/column2';
	
	public $_name;
	
	public $menu = array (
			array('label'=>'home','url'=>array('default/index') ),
			array('label'=>'Simple1','url'=>array('default/simple1') ),
			array('label'=>'Simple2','url'=>array('default/simple2') ),
		);
	
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionSimple1(){
		$upload_dir = 'protected/files/';
		
		$files = scandir($upload_dir);
		
		$this->render('simple1',array(
			'files'=>$files,
		));
	}
	
	public function actionSimpleupload1(){
	
		
			// $myfile = CUploadedFile::getInstancesByName('myfile');
			// CUploadedFile::saveAs('protected/files/');
			 // CUploadedFile::getInstancesByName('myfile');
			 $max_filesize = ini_get('upload_max_filesize');
			 $targetPath = 'protected/files/';
			 
			if (!empty($_FILES)) {
				$filename = $_FILES['myfile']['name'];
				$info=pathinfo($_FILES['myfile']['name']);
				$tempFile = $_FILES['myfile']['tmp_name'];   
				
				
				if(file_exists($targetPath.$_FILES['myfile']['name'])){
					$i = 1;
					$info=pathinfo($_FILES['myfile']['name']);
					while(file_exists($targetPath.$info['filename']."_".$i.".".$info['extension'])) {
						$i++;
					}
					$_FILES['myfile']['name']=$info['filename']."_".$i.".".$info['extension'];
				}
				$targetFile =  $targetPath. $_FILES['myfile']['name']; 
				
				switch ($_FILES['myfile']['error']) {
						case UPLOAD_ERR_OK:
							break;
						case UPLOAD_ERR_NO_FILE:
							throw new RuntimeException('No file sent.');
						case UPLOAD_ERR_INI_SIZE:
						case UPLOAD_ERR_FORM_SIZE:
							//throw new RuntimeException('Exceeded filesize limit.');
							throw new CHttpException(400, 'Exceeded filesize limit');
						default:
							throw new CHttpException(400, 'Unknown errors.');
					}
				//echo $targetFile;
				//echo $tempFile;
				if(move_uploaded_file($_FILES['myfile']['tmp_name'],$targetFile)) {
					//echo $_FILES['myfile']['error']; 
					$this->redirect('simple1');
				} else {
					//echo $_FILES['myfile']['error']; 
					// Check $_FILES['upfile']['error'] value.
					
				};
				
			}
		
		$this->render('simpleupload1',array(
			'max_filesize'=>$max_filesize,
		));
	}
	
	public function actionDelete1(){
		$targetPath = 'protected/files/';
		if(isset($_GET['name'])) {
			$targetPath .= $_GET['name'];
			unlink($targetPath);
		}
		$this->redirect('simple1');
	}
	
	public function actionDownload1(){
		$targetPath = 'protected/files/';
		if(isset($_GET['name'])) {
			$targetPath .= $_GET['name'];
			Yii::app()->getRequest()->sendFile( $_GET['name'] , file_get_contents( $targetPath ) );
		}
	}
	/* BEGIN SIMPLE 2 */
	public function actionSimple2(){
		$upload_dir = 'protected/files/';
		
		$files = scandir($upload_dir);
		
		$this->render('simple2',array(
			'files'=>$files,
		));
	}
	
	/* END SIMPLE 2 */
}