# Yii File Manager Module #

This Module is supposed to be a practice module that will be used as add on to a web app. 

### Target  ###

* [On Progress] Supposed to be Joomla filemanager.
* [Pending] have a component for pop up upload and pick files. just like Responsive FileManager. 
* [Pending And On Research] There are sample on how to use if user only view their files.  

### How do I get set up? ###

* Copy files to modules folder and set the config

### Notes ###

* I'm new to this sharing project
* I will update only when i have time to updates
* Question, Issues, and Documentation will be added later after reaching version 0.0.1 when joomla lookalike file manager is finish.

### Resources ###
  
1. Joomla.  
2. Responsive File Manager.  

