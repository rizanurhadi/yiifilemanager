<?php

class YiifilemanagerModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		
		Yii::app()->theme = 'classic';
		// import the module-level models and components
		$this->setImport(array(
			'yiifilemanager.models.*',
			'yiifilemanager.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			
			if(Yii::app()->user->checkAccess('Admin'))
				return true;
			else 
				throw new CHttpException(400, Rights::t('core', 'Invalid request. Please do not repeat this request again.'));
		}
		else
			return false;
	}
}
